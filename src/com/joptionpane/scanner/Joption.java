package com.joptionpane.scanner;
import javax.swing.*;

public class Joption {
    public static void main(String[] args) {
        // write your code here
        String studentName, inputValue;
        int course1Mark, course2Mark, totalMark;

        studentName = JOptionPane.showInputDialog("Enter your name");
        inputValue = JOptionPane.showInputDialog("Enter course one mark");
        course1Mark = Integer.parseInt(inputValue);
        inputValue = JOptionPane.showInputDialog("Enter course two mark");
        course2Mark = Integer.parseInt(inputValue);
        //Logic Code
        totalMark = course1Mark + course2Mark;
        JOptionPane.showMessageDialog(null, "Name: " + studentName + "\nTotal mark = " + totalMark);
    }
}